# Запросы на создание таблиц

```
CREATE TABLE Clients(
Id INT PRIMARY KEY IDENTITY,
DateOfRegistrationUTC DATETIME2(0) NOT NULL,
Name NVARCHAR(300) NOT NULL
)
GO 


CREATE TABLE Orders(
Id INT PRIMARY KEY IDENTITY,
ClientId INT FOREIGN KEY REFERENCES Clients(Id) NOT NULL,
DateOfFullPaymentUTC DATETIME2(0))

CREATE INDEX Orders_ClientId ON Orders (ClientId);
```

# Запросы на вставку данных

```
INSERT INTO Clients (Name, DateOfRegistrationUTC) VALUES (N'Клиент №1. Без покупок', N'2020-05-28 00:01:00')
INSERT INTO Clients (Name, DateOfRegistrationUTC) VALUES (N'Клиент №2. Без покупок', N'2020-05-28 00:02:00')
INSERT INTO Clients (Name, DateOfRegistrationUTC) VALUES (N'Клиент №3. Первая покупка через 10 дней', N'2020-05-20 00:00:00')
INSERT INTO Clients (Name, DateOfRegistrationUTC) VALUES (N'Клиент №4. Первая покупка через 6 дней', N'2020-05-20 00:00:00')
INSERT INTO Clients (Name, DateOfRegistrationUTC) VALUES (N'Клиент №5. Первая покупка через 5 дней ровно', N'2020-05-20 00:00:00')
INSERT INTO Clients (Name, DateOfRegistrationUTC) VALUES (N'Клиент №6. Первая покупка через 5 дней с часами', N'2020-05-20 00:00:00')
INSERT INTO Clients (Name, DateOfRegistrationUTC) VALUES (N'Клиент №7. Первая покупка в день регистрации', N'2020-05-20 00:00:00')
INSERT INTO Clients (Name, DateOfRegistrationUTC) VALUES (N'Клиент №8. Несколько покупок до 5 дней с даты регистрации', N'2020-05-20 00:00:00')



--Заказы для Клиента №3
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(3, '2020-05-30 00:00:00')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(3, '2020-06-10 00:00:00')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(3, '2020-06-20 00:00:00')

--Заказы для Клиента #4
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(4, '2020-05-26 00:00:00')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(4, '2020-06-10 00:00:00')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(4, '2020-06-20 00:00:00')


--Заказы для клиента №5
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(5, '2020-05-25 00:00:00')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(5, '2020-06-10 00:00:00')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(5, '2020-06-20 00:00:00')

--Заказы для клиента №6
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(6, '2020-05-25 11:11:11')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(6, '2020-06-10 00:00:00')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(6, '2020-06-20 00:00:00')

--Заказы для клиента №7
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(7, '2020-05-20 11:11:11')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(7, '2020-06-10 00:00:00')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(7, '2020-06-20 00:00:00')

--Заказы для клиента №8
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(8, '2020-05-20 11:11:11')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(8, '2020-05-20 15:11:11')
INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(8, '2020-05-21 11:11:11')

```

# Доп. вставка для нагрузки

```
-- для нагрузки чтобы было много записей в таблицах
DECLARE @i INT = 9;

WHILE @i < 1000000
BEGIN
   
   INSERT INTO Clients (Name, DateOfRegistrationUTC) VALUES (N'Клиент №' + CAST(@i as NVARCHAR(300)) + N'. Для нагрузки', N'2020-05-20 00:00:00')

   INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(@i, '2020-08-20 00:00:00')

   SET @i = @i + 1;
   
   INSERT INTO Clients (Name, DateOfRegistrationUTC) VALUES (N'Клиент №' + CAST(@i as NVARCHAR(300)) + N'. Для нагрузки', N'2020-05-20 00:00:00')

   INSERT INTO Orders (ClientId, DateOfFullPaymentUTC) VALUES(@i, '2020-05-21 10:11:11')

   SET @i = @i + 1;
END;
```